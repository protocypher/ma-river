from setuptools import setup


setup(
      name="River",
      version="0.1.0",
      packages=["river"],
      url="https://bitbucket.org/protocypher/ma-river",
      license="MIT",
      author="Benjamin Gates",
      author_email="benjamin@snowmantheater.com",
      description="A bank accounts management application."
)

