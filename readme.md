# River

**Bank Account Manager**

River is a basic, text based, bank account manager that uses the `cmd.Cmd`
framework. It allows clients to move money between their accounts using an
interface similar to ATMs.

